#include <QPainter>
#include <QStaticText>
#include <QDebug>

#include "mediawidget.h"

//// Ctors and Dtors ///////////////////////////////////////////////////////////////////////////////
MediaWidget::MediaWidget(QWidget *parent) :
    QGLWidget(parent)
{
}

MediaWidget::~MediaWidget()
{
    qDebug() << this << "~dtor";
}


//// Events Handlers ///////////////////////////////////////////////////////////////////////////////
void MediaWidget::mouseReleaseEvent(QMouseEvent *e)
{
    QWidget::mouseReleaseEvent(e);
    if (e->button() == Qt::LeftButton)
    {
        emit mouseButtonReleased(e->button());
    }
}

void MediaWidget::paintEvent(QPaintEvent *e)
{
    Q_UNUSED(e);
    QPainter painter;

    bool   isEmpty = false;
    QImage img;
    QImage imgScaled;

    {
        QMutexLocker lock(&frameMutex);
        isEmpty = byteFrame.isEmpty();
        if (isEmpty == false)
        {
            if (!img.loadFromData(byteFrame, "JPEG"))
            {
                QGLWidget::paintEvent(e);
                return;
            }
        }
    }

    painter.begin(this);

    painter.setBrush(Qt::black);
    painter.drawRect(rect());

    if (isEmpty == false)
    {
        imgScaled = img.scaled(width(), height(), Qt::KeepAspectRatio);
        painter.drawImage(QPoint((width() - imgScaled.width()) / 2.0,
                                 (height() - imgScaled.height()) / 2.0),
                          imgScaled);
    }

    painter.setPen(QColor(Qt::white));
    painter.setBrush(QColor(Qt::black));

    //QFont        font;
    //QFontMetrics metric(font);
    //QRect        rect = metric.boundingRect(title);
    painter.drawText(3, height() - 3, title);

    /*QStaticText text(title);
    painter.drawStaticText(3, height() - text.size().height(), text);*/

    painter.end();

    QGLWidget::paintEvent(e);
}

void MediaWidget::enterEvent(QEvent *e)
{
    QWidget::enterEvent(e);
    emit mouseEnter();
}

void MediaWidget::leaveEvent(QEvent *e)
{
    QWidget::leaveEvent(e);
    emit mouseLeave();
}

void MediaWidget::initializeGL()
{
    //glClearColor(0.0, 0.0, 0.0, 0.0);
}


//// Private Methods ///////////////////////////////////////////////////////////////////////////////


//// Public Methods ////////////////////////////////////////////////////////////////////////////////


//// Public Slots //////////////////////////////////////////////////////////////////////////////////
void MediaWidget::updateFrame(const QByteArray &frame)
{
    QMutexLocker lock(&frameMutex);
    byteFrame = frame;
    update();
}

//// Private Slots /////////////////////////////////////////////////////////////////////////////////


