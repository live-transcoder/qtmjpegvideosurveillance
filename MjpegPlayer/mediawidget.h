#ifndef MEDIAWIDGET_H
#define MEDIAWIDGET_H

#include <QWidget>
#include <QtOpenGL/QGLWidget>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QPixmap>
#include <QMutex>
#include <QMutexLocker>

class MediaWidget : public QGLWidget
{
    Q_OBJECT
public:
    explicit MediaWidget(QWidget *parent = 0);
    virtual ~MediaWidget();

    void setTitle(const QString &newTitle) {title = newTitle; repaint();}
    QString &getTitle() {return title;}

signals:
    void mouseButtonReleased(Qt::MouseButton button);
    void mouseEnter();
    void mouseLeave();

public slots:
    void updateFrame(const QByteArray &frame);

protected:
    virtual void mouseReleaseEvent(QMouseEvent *e);
    virtual void paintEvent(QPaintEvent *e);
    virtual void enterEvent(QEvent *e);
    virtual void leaveEvent(QEvent *e);

    void initializeGL();

private:
    QByteArray byteFrame;
    QString    title;
    QMutex     frameMutex;
};

#endif // MEDIAWIDGET_H
