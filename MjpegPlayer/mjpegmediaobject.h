#ifndef MJPEGMEDIAOBJECT_H
#define MJPEGMEDIAOBJECT_H

#include <QApplication>
#include <QObject>
#include <QByteArray>
#include <QPixmap>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QDebug>
#include <QEventLoop>
#include <QAtomicInt>
#include <QSharedPointer>

#include "imagebuffer.h"
#include "workerexecutor.hpp"
#include "mjpegworker.h"



class MjpegMediaObject : public QObject
{
Q_OBJECT

public:
    explicit MjpegMediaObject(const QSharedPointer<ImageBuffer> &buffer, QObject *parent = 0);
    ~MjpegMediaObject();

    void load(const QString &url);
    bool play();
    void stop();

    QString &getUrl() {return url;}
    MediaObjectState getState();



signals:
    void finished();
    void error(int error);



private slots:
    void onFinished();
    void onError(int code);


private:
    //MjpegWorker  *worker;
    WorkerExecutor<MjpegWorker> *executor;

    QString         url;
};

#endif // MJPEGMEDIAOBJECT_H
