#include <QSignalMapper>
#include <QNetworkRequest>
#include <QUrl>
#include <QDebug>


#include "mjpegmediaobject.h"


//// Ctors and Dtors ///////////////////////////////////////////////////////////////////////////////
MjpegMediaObject::MjpegMediaObject(const QSharedPointer<ImageBuffer> &buffer, QObject *parent)
    : QObject(parent)
{
    executor = 0;

    executor = new WorkerExecutor<MjpegWorker>();
    executor->getWorker().setImageBuffer(buffer);
    executor->launch();

    connect(executor->getWorkerPtr(), SIGNAL(finished()),
            this,   SLOT(onFinished()));
    connect(executor->getWorkerPtr(), SIGNAL(error(int)),
            this,   SLOT(onError(int)));
}

MjpegMediaObject::~MjpegMediaObject()
{
    executor->done();
    executor->wait();

    delete executor;
    qDebug() << this << "~dtor";
}


//// Events Handlers ///////////////////////////////////////////////////////////////////////////////


//// Public Methods ////////////////////////////////////////////////////////////////////////////////
void MjpegMediaObject::load(const QString &url)
{
    this->url = url;
    QMetaObject::invokeMethod(&(executor->getWorker()), "load", Qt::QueuedConnection, Q_ARG(QString, url));
}


bool MjpegMediaObject::play()
{
    QMetaObject::invokeMethod(&(executor->getWorker()), "play", Qt::QueuedConnection);
    return true;
}


void MjpegMediaObject::stop()
{
    QMetaObject::invokeMethod(&(executor->getWorker()), "stop", Qt::QueuedConnection);
}

MediaObjectState MjpegMediaObject::getState()
{
    return executor->getWorker().getPlayState();
}



//// Protected Methods /////////////////////////////////////////////////////////////////////////////


//// Private Methods ///////////////////////////////////////////////////////////////////////////////


//// Public Slots //////////////////////////////////////////////////////////////////////////////////


//// Private Slots /////////////////////////////////////////////////////////////////////////////////
void MjpegMediaObject::onFinished()
{
    emit finished();
}


void MjpegMediaObject::onError(int code)
{
    emit error(code);
}
