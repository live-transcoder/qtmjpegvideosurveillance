#include <QStringList>
#include <QRegExp>
#include <QDebug>
#include <QThread>

#include "mjpegworker.h"

MjpegWorker::MjpegWorker(/*const QSharedPointer<ImageBuffer> &imageBuffer*/) :
    QObject(0)
{
    manager = new QNetworkAccessManager(this);
    state   = STATE_STOP;
    //buffer  = imageBuffer;
    reply   = 0;
    isFirstPacket = true;
}


MjpegWorker::~MjpegWorker()
{
    // network manager object and all replies will be released in ~QObject dtor as childres items
    // so we not needed free it here.

    qDebug() << "current thread: " << QThread::currentThread() << "this thread: " << thread() << "net manager thread: " << manager->thread();
    if (reply)
    {
        qDebug() << "reply thread: " << reply->thread();
    }

    qDebug() << this << "~dtor";
}


MediaObjectState MjpegWorker::getPlayState()
{
    return state;
}


bool MjpegWorker::setImageBuffer(const QSharedPointer<ImageBuffer> &imageBuffer)
{
    if (buffer.isNull())
    {
        buffer = imageBuffer;
        return true;
    }

    return false;
}


void MjpegWorker::load(const QString &url)
{
    this->url = url;
}


void MjpegWorker::play()
{
    if (state == STATE_PLAY)
    {
        qDebug() << "Already in play state";
        return;
    }

    QMutexLocker locker(&replyMutex);

    //qDebug() << "Play: " << url;
    isFirstPacket = true;
    currentData.clear();
    QNetworkRequest req;
    req.setUrl(QUrl(url));
    reply = manager->get(req);

    //qDebug() << "Connect signals...";

    connect(reply, SIGNAL(readyRead()),
            this,  SLOT(onDataAvail()));
    connect(reply, SIGNAL(finished()),
            this,  SLOT(onFinished()));
    connect(reply, SIGNAL(error(QNetworkReply::NetworkError)),
            this,  SLOT(onError(QNetworkReply::NetworkError)));

    state = STATE_PLAY;
}


void MjpegWorker::stop()
{
    if (state == STATE_STOP)
    {
        qDebug() << "Already in stop state";
        return;
    }

    QMutexLocker locker(&replyMutex);

    if (reply)
    {
        reply->close();
    }
    else
    {
        state = STATE_STOP;
    }
}


void MjpegWorker::onFinished()
{
    state = STATE_STOP;
    qDebug() << "Finished:" << url << ", Error: " << reply->errorString() << "(" << reply->error() << ")";
    reply = 0;
    emit finished();
}


void MjpegWorker::onDataAvail()
{
    // <<<
    static QString boundary;
    if (isFirstPacket)
    {
        boundary = QString();

        QString        contentType = reply->rawHeader("Content-Type");
        QStringList    contentTypeElems = contentType.split(";", QString::SkipEmptyParts);

        for (int i = 0; i < contentTypeElems.count(); ++i)
        {
            QString elem = contentTypeElems.at(i).trimmed();
            if (elem.contains(QRegExp("^multipart/x-mixed-replace")))
            {
                //isMultiPart = true;
            }
            else if (elem.contains(QRegExp("^boundary=")))
            {
                boundary = elem.split("boundary=").at(1);
            }
        }
        isFirstPacket = false;
    }
    // >>>

    QByteArray data = reply->readAll();

    currentData.append(data);

    int idx = currentData.indexOf(boundary.toAscii());

    if (idx == -1)
    {
        return;
    }

    // Drop wrong data (?)
    if (idx > 0)
    {
        currentData.remove(0, idx);
    }

    // Начало фрейма теперь в нулевой позиции
    FrameSizeInfo info = getFrameSizeInfo(currentData, 0);
    if (info.beginPos == -1)
    {
        // Надо бы поднакопить данные
        return;
    }

    // Поглядим, есть ли у нас данные для нового фрейма
    if (info.beginPos + info.fullSize > currentData.size())
    {
        // Надо бы поднакопить данные
        return;
    }

    // Сформируем кадр
    QByteArray frame = currentData.mid(info.beginPos, info.fullSize);
    currentData.remove(0, info.beginPos + info.fullSize);

    if (buffer)
    {
        buffer->addFrame(frame);
    }

    emit newFrame(frame);
}


void MjpegWorker::onError(QNetworkReply::NetworkError code)
{
    emit error(code);
}


MjpegWorker::FrameSizeInfo MjpegWorker::getFrameSizeInfo(QByteArray &buffer, qint64 offset)
{
    FrameSizeInfo info(-1,-1,-1);

    QMap<QString, QString> headers;
    // Обработаем заголовки и найдём начало данным (после одной пустой строки)
    bool isDataFound = false;
    int newLineIdx     = offset;
    int newLineIdxPrev = offset;
    while (true)
    {
        newLineIdxPrev = newLineIdx + 1;
        newLineIdx = buffer.indexOf("\r\n", newLineIdxPrev);
        //qDebug() << newLineIdxPrev << newLineIdx;

        if (newLineIdx == -1)
        {
            break;
        }

        int diff = newLineIdx - newLineIdxPrev;
        QString header = buffer.mid(newLineIdxPrev - 1, diff + 1).trimmed();
        //qDebug() << "RAW HEADER: " << header;

        QRegExp exp("^([a-zA-Z-]+):(.+)$");
        int     pos = exp.indexIn(header);
        QString key;
        QString value;
        if (pos > -1)
        {
            key = exp.cap(1).trimmed();
            value = exp.cap(2).trimmed();

            if (key.compare("Content-Length") == 0)
            {
                info.fullSize = value.toInt();
            }

            headers.insert(key, value);
        }

        if (diff == 1)
        {
            isDataFound = true;
            break;
        }
    }

    //qDebug() << headers << isDataFound;

    if (isDataFound)
    {
        info.beginPos = newLineIdx + 2;
        info.blockSize = buffer.size() - info.beginPos;
    }

    return info;
}


