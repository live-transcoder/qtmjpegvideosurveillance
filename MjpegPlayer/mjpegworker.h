#ifndef MJPEGWORKER_H
#define MJPEGWORKER_H

#include <QObject>
#include <QNetworkReply>
#include <QMutex>
#include <QSharedPointer>


#include "imagebuffer.h"


enum MediaObjectState {
    STATE_PLAY,
    STATE_STOP
};


/**
 * @brief The MjpegWorker class - mjpeg threaded worker
 */
class MjpegWorker : public QObject
{
    Q_OBJECT

private:
    struct FrameSizeInfo
    {
        FrameSizeInfo(qint64 beginPos = -1, qint64 fullSize = -1, qint64 blockSize = -1)
            : beginPos(beginPos),
              fullSize(fullSize),
              blockSize(blockSize) {}

        qint64 beginPos;
        qint64 fullSize;
        qint64 blockSize;
    };

public:
    explicit MjpegWorker(/*const QSharedPointer<ImageBuffer> &imageBuffer*/);
    ~MjpegWorker();

    MediaObjectState getPlayState();
    bool setImageBuffer(const QSharedPointer<ImageBuffer> &imageBuffer);


signals:
    void newFrame(const QByteArray &frame);
    void finished();
    void error(int error);


public slots:
    void load(const QString &url);
    void play();
    void stop();

private slots:
    void onFinished();
    void onDataAvail();
    void onError(QNetworkReply::NetworkError code);


private:
    FrameSizeInfo getFrameSizeInfo(QByteArray &buffer,
                                   qint64 offset);

private:
    MediaObjectState                    state;

    QString                             url;

    QNetworkAccessManager              *manager;
    QNetworkReply                      *reply;

    QByteArray                          currentData;
    bool                                isFirstPacket;

    QMutex                              replyMutex;

    QSharedPointer<ImageBuffer>         buffer;

};

#endif // MJPEGWORKER_H
