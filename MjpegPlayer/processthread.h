#ifndef PROCESSTHREAD_H
#define PROCESSTHREAD_H

#include <QThread>
#include <QAtomicInt>
#include <QSharedPointer>

#include "imagebuffer.h"
#include "mediawidget.h"

class ProcessThread : public QThread
{
    Q_OBJECT
public:
    explicit ProcessThread(QSharedPointer<ImageBuffer> buffer, QObject *parent = 0);
    virtual ~ProcessThread();

    void setRenderWidget(MediaWidget *widget);
    void setPaused(bool paused);
    void setFrameDropping(int frameDrop);

    void done();

protected:
    void run();

signals:

public slots:

private:
    QSharedPointer<ImageBuffer> buffer;
    MediaWidget *widget;
    QAtomicInt   paused;
    QAtomicInt   isDone;
    QAtomicInt   frameDrop;

    QMutex       widgetChangeMutex;
};

#endif // PROCESSTHREAD_H
