#ifndef MEDIACONTROLL_H
#define MEDIACONTROLL_H

#include <QSharedPointer>

#include "mjpegmediaobject.h"
#include "mediawidget.h"
#include "imagebuffer.h"
#include "processthread.h"

#include "ui_mediacontroll.h"

class MediaControll : public QWidget, private Ui::MediaControll
{
    Q_OBJECT

public:
    explicit MediaControll(QWidget *parent = 0);
    ~MediaControll();

    /**
     * @brief load - load new source URL without starting playing
     * @param url   url poited to mjpeg stream
     */
    void load(const QString &url);

    /**
     * @brief play - connect to server and start playback
     */
    void play();

    /**
     * @brief stop - disconnect from server and stop playback
     */
    void stop();

    /**
     * @brief pause - continue receiving data from server, but don't update (aka process) it
     * @param pause - pause or/unpause playback
     */
    void pause(bool pause);


    /**
     * @brief setFrameDrop - set amount of frames to drop between redraw
     * @param frameDrop - amount of frames to drop between redraw, 0 - no frame-drop, 1 - full drop
     */
    void setFrameDropping(int frameDrop);

    /**
     * @brief setTitle - set title for video port label
     * @param title - new title
     */
    void setTitle(const QString &title);


    /**
     * @brief setAutoReplay - set auto replay mode
     * @param doAutoReplay  auto replay mode, if true - replay media after finished
     */
    void setAutoReplay(bool autoRepeat);

signals:

    /**
     * @brief finished - media completed
     *        this signal emited when media complete playing (for ex, server close connection)
     */
    void finished();

    /**
     * @brief mouseClicked - mouse clicked inside media widget
     * @param button - mouse button
     * @param url    - camera URL
     * @param desc   - camera description
     */
    void mouseClicked(Qt::MouseButton button, const QString &url, const QString &desc);

    /**
     * @brief httpError - report HTTP error
     * @param errorCode - http error code
     * @param url       - camera URL
     * @param desc      - camera description
     */
    void httpError(int errorCode, const QString &url, const QString &desc);

protected:
    void changeEvent(QEvent *e);

private slots:
    void on_reloadButton_clicked();
    void on_playButton_clicked();
    void on_stopButton_clicked();
    void onFinished();
    void onError(int error);

    void onVideoPortMouseEnter();
    void onVideoPortMouseLeave();
    void onVideoPortClicked(Qt::MouseButton button);

private:
    MjpegMediaObject *mediaObject;
    ProcessThread    *processThread;
    bool              doReload;
    bool              autoRepeat;
    bool              forceStop;
    bool              isPaused;
    QSharedPointer<ImageBuffer> buffer;
};

#endif // MEDIACONTROLL_H
