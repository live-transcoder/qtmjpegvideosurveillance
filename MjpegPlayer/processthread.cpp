#include <QMutexLocker>
#include <QDebug>

#include "processthread.h"

ProcessThread::ProcessThread(QSharedPointer<ImageBuffer> buffer, QObject *parent) :
    QThread(parent),
    buffer(buffer),
    widget(0),
    paused(0),
    frameDrop(0)
{
}

ProcessThread::~ProcessThread()
{
    qDebug() << this << "~dtor";
}

void ProcessThread::run()
{
    int frameCount = 0;
    isDone = 0;
    while (!isDone)
    {
        if (buffer)
        {
            QByteArray frame = buffer->getFrame(100);

            if (frame.isEmpty())
            {
                continue;
            }

            if (widget && paused == 0)
            {
                if (frameDrop == 0 || frameCount % frameDrop == 0)
                {
                    widget->updateFrame(frame);
                }
            }

            ++frameCount;
        }
    }
}

void ProcessThread::setRenderWidget(MediaWidget *widget)
{
    QMutexLocker lock(&widgetChangeMutex);
    this->widget = widget;
}

void ProcessThread::setPaused(bool paused)
{
    if (paused)
    {
        this->paused = 1;
    }
    else
    {
        this->paused = 0;
    }
}

void ProcessThread::setFrameDropping(int frameDrop)
{
    this->frameDrop = frameDrop;
}

void ProcessThread::done()
{
    isDone = 1;
}
