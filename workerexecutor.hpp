#ifndef WORKEREXECUTOR_H
#define WORKEREXECUTOR_H

#include <QObject>
#include <QMutex>
#include <QWaitCondition>
#include <QSignalMapper>
#include <QThread>
#include <QDebug>
#include <QSharedPointer>

/**
 * @brief The WorkerExecutorInternal class
 * The base class to templated WorkerExecutor class. It used to split Q_OBJECT declaration,
 * that can't be templated and template functionality
 */
class WorkerExecutorInternal : public QThread
{
    Q_OBJECT

public:
    explicit WorkerExecutorInternal(QObject *parent = 0) : QThread(parent)
    {
        shutdownHelper = new QSignalMapper;

        shutdownHelper->setMapping(this, 0);
        connect(this, SIGNAL(aboutToStop()), shutdownHelper, SLOT(map()));

        connect(this, SIGNAL(started()),    this, SLOT(threadReady()),  Qt::DirectConnection);
        connect(this, SIGNAL(terminated()), this, SLOT(onTerminated()), Qt::DirectConnection);
        connect(this, SIGNAL(finished()),   this, SLOT(onFinished()),   Qt::DirectConnection);
    }

    virtual ~WorkerExecutorInternal()
    {
        delete shutdownHelper;
        qDebug() << this << "~dtor";
    }


    /**
     * @brief done - finish task execution.
     *
     * Exit from EventLoop. To synchronize thread stopping you can call 'wait()' method
     */
    void done()
    {
        emit aboutToStop();
    }


signals:
    void aboutToStop();


protected:
    void launch()
    {
        shutdownHelper->moveToThread(this);
        connect(shutdownHelper, SIGNAL(mapped(int)), this, SLOT(stopExecutor()), Qt::DirectConnection);
    }

    /**
     * @brief onThreadFinish - call after thread finish. Can be reimplemented in child classes.
     */
    virtual void onThreadFinish()    {}

    /**
     * @brief onThreadStart - call after thread start. Can be reimplemented in child classes.
     */
    virtual void onThreadStart()     {}

    /**
     * @brief onThreadTerminate - call after thread termination. Can be reimplemented in child classes.
     */
    virtual void onThreadTerminate() {}

private slots:
    void stopExecutor()
    {
        exit();
    }

    void threadReady()
    {
        qDebug() << "thread ready: " << currentThreadId();
        onThreadStart();
    }

    void onTerminated()
    {
        qDebug() << "thread terminated: " << currentThreadId();
        onThreadTerminate();
    }

    void onFinished()
    {
        qDebug() << "thread finished: " << currentThreadId();
        onThreadFinish();
    }

private:
    QSignalMapper *shutdownHelper;

};



/**
 * @brief The WorkerExecutor class.
 * Execute task in new thread. Task class should delivered from QObject.
 */
template<typename T>
class WorkerExecutor : public WorkerExecutorInternal
{
public:
    explicit WorkerExecutor(QObject *parent = 0)
        : WorkerExecutorInternal(parent)
    {
        worker = new T;
    }


    ~WorkerExecutor()
    {
        delete worker;

        qDebug() << "WorkerExecutor(" << (void*)this << ")" << "~dtor";
    }


    /**
     * @brief launch - start task execution in new thread
     */
    void launch()
    {
        start();
        oldWorkerThread = worker->thread();
        worker->moveToThread(this);

        WorkerExecutorInternal::launch();
    }


    /**
     * @brief getWorker - access to worker instance
     * @return reference to worker instance
     */
    T &getWorker()
    {
        return (*worker);
    }


    /**
     * @brief getWorkerPtr - access to worker instance
     * @return  const pointer to worker instance
     */
    const T *getWorkerPtr()
    {
        return worker;
    }

protected:
    void onThreadFinish()
    {
        // return worker back to created thread
        worker->moveToThread(oldWorkerThread);
    }

private:
    T             *worker;
    QThread       *oldWorkerThread;

};

#endif // WORKEREXECUTOR_H
