#ifndef CAMSLIST_H
#define CAMSLIST_H

#include <QObject>
#include <QList>

struct CamInfo
{
    QString url;
    QString description;
};

class CamsList : public QObject
{
    Q_OBJECT
public:
    explicit CamsList(QObject *parent = 0);

    void update();
    const QList<CamInfo> &getCams();

    const CamInfo &getCamByUrl(const QString &url);
    
signals:
    
public slots:
    
private:
    QList<CamInfo> camsList;
    CamInfo        empty;
};

#endif // CAMSLIST_H
