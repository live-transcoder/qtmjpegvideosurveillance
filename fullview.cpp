#include "fullview.h"

FullView::FullView(const QString &url, const QString &desc, QWidget *parent) :
    QDialog(parent)
{
    setupUi(this);
    mediaControll->load(url);
    mediaControll->setTitle(desc);
    mediaControll->play();
}

FullView::~FullView()
{
    mediaControll->stop();
}

void FullView::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
        case QEvent::LanguageChange:
            retranslateUi(this);
            break;
        default:
            break;
    }
}
