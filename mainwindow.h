#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QSignalMapper>

#include "ui_mainwindow.h"
#include "camslist.h"
#include "camview/camview.h"

class MainWindow : public QMainWindow, private Ui::MainWindow
{
Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

protected:
    void changeEvent(QEvent *e);
    void closeEvent(QCloseEvent *e);

signals:
    void presetListUpdated();

private slots:
    void onViewDestroyed(QWidget *obj);
    void on_actionQuit_triggered();
    void on_actionConfigureViews_triggered();
    void on_actionAddNewView_triggered();

private:
    CamsList        *camsList;

    CamView         *mainView;
    QMap<CamView*, QString> views;

    QSignalMapper   *viewDestroyMapper;
};

#endif // MAINWINDOW_H
