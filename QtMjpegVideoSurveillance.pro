#-------------------------------------------------
#
# Project created by QtCreator 2011-06-07T13:58:38
#
#-------------------------------------------------

QT       += core gui network opengl

TARGET = QtMjpegVideoSurveillance
TEMPLATE = app

INCLUDEPATH += MjpegPlayer

SOURCES += main.cpp\
        mainwindow.cpp \
    MjpegPlayer/mjpegmediaobject.cpp \
    MjpegPlayer/mediawidget.cpp \
    MjpegPlayer/mediacontroll.cpp \
    MjpegPlayer/imagebuffer.cpp \
    MjpegPlayer/processthread.cpp \
    settings.cpp \
    camslist.cpp \
    fullview.cpp \
    camview/camview.cpp \
    viewpresets/viewpresets.cpp \
    MjpegPlayer/mjpegworker.cpp

HEADERS  += mainwindow.h \
    MjpegPlayer/mjpegmediaobject.h \
    MjpegPlayer/mediawidget.h \
    MjpegPlayer/mediacontroll.h \
    MjpegPlayer/imagebuffer.h \
    MjpegPlayer/processthread.h \
    settings.h \
    camslist.h \
    fullview.h \
    camview/camview.h \
    viewpresets/viewpresets.h \
    MjpegPlayer/mjpegworker.h \
    workerexecutor.hpp

FORMS    += mainwindow.ui \
    MjpegPlayer/mediacontroll.ui \
    fullview.ui \
    camview/camview.ui \
    viewpresets/viewpresets.ui

RESOURCES += \
    MjpegPlayer/mediacontrol.qrc \
    main.qrc











