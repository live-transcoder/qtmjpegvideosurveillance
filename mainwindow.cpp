#include <math.h>

#include <memory>

#include <QDebug>
#include <QRegExp>

#include "mainwindow.h"
#include "MjpegPlayer/mediacontroll.h"
#include "settings.h"
#include "camslist.h"
#include "fullview.h"
#include "viewpresets/viewpresets.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent)
{
    setupUi(this);

    Settings *cfg = Settings::instance();

    // Restore geometry
    QByteArray geometry = cfg->getParam("views.main_view.geometry").toByteArray();
   restoreGeometry(geometry);

    // Mjpeg sources
    camsList = new CamsList(this);
    mainView = new CamView(camsList, "main_view", mainWidget);

    mainWidget->setLayout(new QVBoxLayout(this));
    mainWidget->layout()->addWidget(mainView);
    mainWidget->layout()->setContentsMargins(0, 0, 0, 0);

    connect(this,     SIGNAL(presetListUpdated()),
            mainView, SLOT(onPresetListUpdate()));

    // Set up signal mapper for view destroy signals
    viewDestroyMapper = new QSignalMapper(this);
    connect(viewDestroyMapper, SIGNAL(mapped(QWidget*)),
            this,              SLOT(onViewDestroyed(QWidget*)));

    // Load Views
    QStringList viewIds = cfg->getParam("views.ids").toStringList();
    for (int i = 0; i < viewIds.size(); ++i)
    {
        CamView *view = new CamView(camsList, viewIds.at(i));
        //views.append(view);
        views.insert(view, view->getId());

        connect(view,              SIGNAL(destroyed()),
                viewDestroyMapper, SLOT(map()));
        viewDestroyMapper->setMapping(view, view);
        view->show();
    }
}


MainWindow::~MainWindow()
{
    camsList->deleteLater();
}


void MainWindow::changeEvent(QEvent *e)
{
    QMainWindow::changeEvent(e);
    switch (e->type())
    {
        case QEvent::LanguageChange:
            retranslateUi(this);
            break;

        default:
            break;
    }
}


void MainWindow::closeEvent(QCloseEvent *e)
{
    Settings *cfg = Settings::instance();

    // Save main view
    cfg->setParam("views.main_view.preset_id", mainView->getCurrentPresetId());
    cfg->setParam("views.main_view.geometry",  saveGeometry());

    // Save views params
    QStringList viewIds;
    QMap<CamView*, QString>::iterator it;
    //for (int i = 0; i < views.size(); ++i)
    for (it = views.begin(); it != views.end(); ++it)
    {
        //views.at(i)->save();
        //viewIds.append(views.at(i)->getId());

        it.key()->save();
        viewIds.append(it.value());

        // Remove signal mapping
        //viewDestroyMapper->removeMappings(views.at(i));
        viewDestroyMapper->removeMappings(it.key());
    }

    // Set current present Ids
    cfg->setParam("views.ids", viewIds);

    // Save
    cfg->save();

    qDebug() << "Settings sav";

    QMainWindow::closeEvent(e);
}


void MainWindow::on_actionQuit_triggered()
{
    close();
}


void MainWindow::on_actionConfigureViews_triggered()
{
    ViewPresets *p = new ViewPresets((*camsList), this);
    if (p->exec() == QDialog::Accepted)
    {
        Settings::instance()->save();
        emit presetListUpdated();
    }
    p->deleteLater();
}


void MainWindow::on_actionAddNewView_triggered()
{
    CamView *view = new CamView(camsList);
    //views.append(view);
    views.insert(view, view->getId());

    connect(view,              SIGNAL(destroyed()),
            viewDestroyMapper, SLOT(map()));
    viewDestroyMapper->setMapping(view, view);

    view->show();
}


void MainWindow::onViewDestroyed(QWidget *obj)
{
    // Delete it from view list
    CamView *view = static_cast<CamView*>(obj);

    QString id = views.value(view);

    qDebug() << "remove view: " << id;

    // Remove from Configuration manager
    Settings *cfg = Settings::instance();
    cfg->removeParam("views." + id + ".preset_id");
    cfg->removeParam("views." + id + ".geometry");

    //views.removeOne(view);
    views.remove(view);
}
