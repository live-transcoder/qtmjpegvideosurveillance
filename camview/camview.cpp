#include <QUuid>

#include "camview.h"
#include "fullview.h"
#include "MjpegPlayer/mediacontroll.h"

CamView::CamView(CamsList *camsList, QWidget *parent) :
    QWidget(parent),
    cfg(Settings::instance()),
    camsList(camsList)
{
    init();
    // It new view, Generate id
    id = QCryptographicHash::hash(QUuid::createUuid().toString().toUtf8(), QCryptographicHash::Sha1).toHex();
}


CamView::CamView(CamsList *camsList, const QString &id, QWidget *parent) :
    QWidget(parent),
    id(id),
    cfg(Settings::instance()),
    camsList(camsList)
{
    init();
}


void CamView::setCurrentPreset(const QString &presetId)
{
    Q_UNUSED(presetId);
    // TODO
}


void CamView::init()
{
    setupUi(this);

    // Delete window on close
    setAttribute(Qt::WA_DeleteOnClose, true);
    setAttribute(Qt::WA_QuitOnClose,   false);

    layout = new QGridLayout(viewWidget);
    layout->setContentsMargins(0, 0, 0, 0);

    onPresetListUpdate();

    // Load views configuration
    QByteArray geometry  = cfg->getParam("views." + id + ".geometry").toByteArray();
    restoreGeometry(geometry);

    QString presetId = cfg->getParam("views." + id + ".preset_id").toString();
    if (presetId.isEmpty() == false)
    {
        for (int i = 0; i < presets.size(); ++i)
        {
            if (presets[i].id == presetId)
            {
                viewPresets->setCurrentIndex(i + 1);
                on_viewPresets_activated(i + 1);
                break;
            }
        }
    }
}


void CamView::save()
{
    cfg->setParam("views." + id + ".preset_id", currentPresetId);
    cfg->setParam("views." + id + ".geometry", saveGeometry());
}


void CamView::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type())
    {
        case QEvent::LanguageChange:
            retranslateUi(this);
            break;
        default:
            break;
    }
}


void CamView::onPresetListUpdate()
{
    int activatePreset = -1;

    presets.clear();
    viewPresets->clear();
    viewPresets->addItem(tr("--- empty ---"));

    QStringList presetIds = cfg->getParam("presets.ids").toStringList();
    for (int i = 0; i < presetIds.size(); ++i)
    {
        const QString &presetId = presetIds.at(i);
        ViewItem preset;
        preset.id = presetId;
        preset.name = cfg->getParam("presets." + presetId + ".name").toString();
        preset.columns = cfg->getParam("presets." + presetId + ".columns").toInt();
        preset.camUrls = cfg->getParam("presets." + presetId + ".camUrls").toStringList();

        viewPresets->addItem(preset.name);

        if (preset.id == currentPresetId)
        {
            activatePreset = i;
        }

        presets.append(preset);
    }

    // First item always "---"
    viewPresets->setCurrentIndex(activatePreset + 1);
    on_viewPresets_activated(activatePreset + 1);
}


void CamView::onMediaControlMouseClicked(Qt::MouseButton button, const QString &url, const QString &desc)
{
    Q_UNUSED(button);

    qDebug() << url << desc;
    FullView *view = new FullView(url, desc, this);

    for (int i = 0; i < players.size(); ++i)
    {
        MediaControll *ctl = qobject_cast<MediaControll*>(players.at(i));
        ctl->pause(true);
    }

    view->exec();

    for (int i = 0; i < players.size(); ++i)
    {
        MediaControll *ctl = qobject_cast<MediaControll*>(players.at(i));
        ctl->pause(false);
    }

    view->deleteLater();
}


void CamView::on_viewPresets_currentIndexChanged(int index)
{
    // TODO
    qDebug() << "Current index changed: " << index;
}


void CamView::on_viewPresets_activated(int index)
{
    // TODO
    qDebug() << "Index activated: " << index;

    if (index < 0)
    {
        return;
    }

    for (int i = 0; i < players.size(); ++i)
    {
        players.at(i)->deleteLater();
    }
    players.clear();

    if (index == 0)
    {
        currentPresetId = QString(); // empty preset
        return;
    }

    const ViewItem &preset = presets.at(index - 1);
    int cols = preset.columns;
    currentPresetId = preset.id;

    //qDebug() << "Cams count: " << cams.count() << ", cols: " << cols;
    for (int i = 0; i < preset.camUrls.size(); ++i)
    {
        const QString &url = preset.camUrls.at(i);
        CamInfo info = camsList->getCamByUrl(url);

        if (info.url.isEmpty())
        {
            continue;
        }

        MediaControll *video = new MediaControll(viewWidget);
        players.append(video);
        layout->addWidget(video, i / cols, i % cols, 1, 1);
        video->setFrameDropping(Settings::instance()->getParam("core.frame.drop").toInt());
        video->setTitle(info.description);
        video->setAutoReplay(true);
        video->load(info.url);
        video->play();

        connect(video, SIGNAL(mouseClicked(Qt::MouseButton,QString,QString)),
                this,  SLOT(onMediaControlMouseClicked(Qt::MouseButton,QString,QString)));
    }
}
