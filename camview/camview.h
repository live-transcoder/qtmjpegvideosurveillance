#ifndef CAMVIEW_H
#define CAMVIEW_H

#include "ui_camview.h"
#include "settings.h"
#include "camslist.h"
#include "viewpresets/viewpresets.h"

class CamView : public QWidget, private Ui::CamView
{
    Q_OBJECT
    
public:
    explicit CamView(CamsList *camsList, QWidget *parent = 0);
    CamView(CamsList *camsList, const QString &id, QWidget *parent = 0);

    const QString &getId() {return id;}
    const QString &getCurrentPresetId() {return currentPresetId;}

    void setCurrentPreset(const QString &presetId);

public slots:
    void save();

protected:
    void changeEvent(QEvent *e);


public slots:
    void onPresetListUpdate();


private slots:
    void onMediaControlMouseClicked(Qt::MouseButton button, const QString &url, const QString &desc);
    void on_viewPresets_currentIndexChanged(int index);
    void on_viewPresets_activated(int index);


private:
    void init();


private:
    QGridLayout *layout;
    QList<QWidget*> players;
    QString id;
    QString currentPresetId;
    Settings *cfg;
    CamsList *camsList;
    QList<ViewItem> presets;
};

#endif // CAMVIEW_H
