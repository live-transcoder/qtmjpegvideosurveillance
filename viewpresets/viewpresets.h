#ifndef VIEWPRESETS_H
#define VIEWPRESETS_H

#include <QList>
#include <QPair>

#include "ui_viewpresets.h"
#include "settings.h"
#include "camslist.h"

struct CamItem
{
    CamInfo info;
    bool    isChecked;
};

struct ViewItem
{
    QString     id;
    QString     name;
    int         columns;
    QStringList camUrls;

    bool        isUpdated;
    bool        isNew;
};


class ViewPresets : public QDialog, private Ui::ViewPresets
{
    Q_OBJECT
    
public:
    explicit ViewPresets(CamsList &cams, QWidget *parent = 0);
    
protected:
    void changeEvent(QEvent *e);

private slots:
    void on_buttonBox_accepted();
    void on_buttonBox_rejected();
    void on_addView_clicked();
    void on_replaceView_clicked();
    void on_removeView_clicked();
    void on_viewList_itemSelectionChanged();

private:
    void fillViewItem(ViewItem &viewItem);

private:
    Settings        *cfg;
    QList<ViewItem>  presets;
    QList<ViewItem>  removed;
    QList<CamInfo>   camsInfo;

    int              selectedView;
};

#endif // VIEWPRESETS_H
