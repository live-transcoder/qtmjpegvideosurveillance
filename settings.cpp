/**************************************************************************
**   Copyright (C) 2010 by hatred
**   hatred@inbox.ru
**   http://code.google.com/p/photodoc-ng/
**
**   This file is a part of "PhotoDoc NG" application
**
**   This program is free software; you can redistribute it and/or modify
**   it under the terms of the version 2 of GNU General Public License as
**   published by the Free Software Foundation.
**
**   For more information see LICENSE and LICENSE.ru files
**
**   @file   settings.cpp
**   @date   2010-10-31
**   @author hatred
**   @brief  Common application settings: load, save, access via single
**           instanse
**
**************************************************************************/

#include <QCoreApplication>
#include <QSettings>
#include <QFileInfo>
#include <QDebug>

#include "settings.h"

Settings::Settings()
{}

Settings *Settings::instance()
{
    static Settings *instance = new Settings();
    return instance;
}

void Settings::load(const QString& file_name)
{
    QString fname = file_name;
    if (file_name.isEmpty())
    {
        fname = getDefaultConfig();
    }
    QSettings cfg(fname, QSettings::IniFormat);

    QStringList keys = cfg.allKeys();
    for (int i = 0; i < keys.count(); i++)
    {
        const QString &cfg_key = keys.at(i);
        QString key = cfg_key;
        key.replace('/', '.');

        setParam(key, cfg.value(cfg_key));
    }

    //qDebug() << this << _settings;

}


void Settings::save(const QString& file_name)
{
    QString fname = file_name;
    if (file_name.isEmpty())
    {
        fname = getDefaultConfig();
    }
    QSettings cfg(fname, QSettings::IniFormat);

    for (int i = 0; i < _removedKeys.size(); ++i)
    {
        const QString &key = _removedKeys.at(i);
        QString cfg_key = key;
        cfg_key.replace('.', '/');

        cfg.remove(cfg_key);
    }

    _removedKeys.clear();

    QStringList keys = _settings.keys();
    for (int i = 0; i < keys.count(); ++i)
    {
        QString key     = keys.at(i);
        QString cfg_key = key;
        cfg_key.replace(".", "/");

        cfg.setValue(cfg_key, getParam(key));
    }
}

QString Settings::getDefaultConfig()
{
    QString app_name;
    QString company_name;

    // Подберем имя компании, можем его и не использовать
    if (!_company_name.isEmpty())
    {
        company_name = _company_name;
    }
    else
    {
        company_name = qApp->organizationName();
    }

    // А вот имя приложения мы должны по любому сформировать
    if (!_application_name.isEmpty())
    {
        app_name = _application_name;
    }
    else if (!qApp->applicationName().isEmpty())
    {
        app_name = qApp->applicationName();
    }
    else
    {
        QString file = qApp->arguments().at(0);
        QFileInfo fi(file);
        app_name = fi.baseName();
    }

    QSettings cfg(QSettings::IniFormat, QSettings::UserScope, company_name, app_name);

    //qDebug() << "Settings file: " << cfg.fileName();

    return cfg.fileName();
}

void Settings::setAppName(const QString& name)
{
    _application_name = name;
}

void Settings::setCompanyName(const QString& name)
{
    _company_name = name;
}

QVariant Settings::getParam(const QString& key)
{
    return _settings.value(key);
}

void Settings::setParam(const QString& key, const QVariant& value)
{
    _settings.insert(key, value);
}

void Settings::removeParam(const QString &key)
{
    _settings.remove(key);
    _removedKeys.append(key);
}

void Settings::clearParams()
{
    _settings.clear();
}

QStringList Settings::getParamKeys()
{
    return _settings.keys();
}

bool Settings::isParamExists(const QString &key)
{
    return _settings.contains(key);
}
