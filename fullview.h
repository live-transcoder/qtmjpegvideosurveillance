#ifndef FULLVIEW_H
#define FULLVIEW_H

#include "ui_fullview.h"

class FullView : public QDialog, private Ui::FullView
{
    Q_OBJECT
    
public:
    explicit FullView(const QString &url, const QString &desc, QWidget *parent = 0);
    ~FullView();
    
protected:
    void changeEvent(QEvent *e);
};

#endif // FULLVIEW_H
