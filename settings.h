/**************************************************************************
**   Copyright (C) 2010 by hatred
**   hatred@inbox.ru
**   http://code.google.com/p/photodoc-ng/
**
**   This file is a part of "PhotoDoc NG" application
**
**   This program is free software; you can redistribute it and/or modify
**   it under the terms of the version 2 of GNU General Public License as
**   published by the Free Software Foundation.
**
**   For more information see LICENSE and LICENSE.ru files
**
**   @file   settings.h
**   @date   2010-10-31
**   @author hatred
**   @brief  Common application settings: load, save, access via single
**           instanse
**
**************************************************************************/

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QtCore>

class Settings
{
public:
    Settings();

    static Settings *instance();

    void load(const QString& file_name = QString());
    void save(const QString& file_name = QString());

    QString getDefaultConfig();
    void    setAppName(const QString& name);
    void    setCompanyName(const QString& name);

    QVariant    getParam(const QString& key);
    void        setParam(const QString& key, const QVariant& value);
    void        removeParam(const QString& key);
    void        clearParams();
    QStringList getParamKeys();
    bool        isParamExists(const QString &key);

private:
    QMap<QString, QVariant> _settings;
    QString                 _application_name;
    QString                 _company_name;
    QStringList             _removedKeys;
};

#endif // SETTINGS_H
