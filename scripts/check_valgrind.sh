#!/bin/bash

BUILD_DIR=QtMjpegVideoSurveillance-build-desktop-Qt_4.8.0-gdb_Debug
#BUILD_DIR=QtMjpegVideoSurveillance-build-desktop-Qt_in_PATH_Debug

[ "$1" == "-g" ] && ADD="--db-attach=yes"

export LD_LIBRARY_PATH=/opt/qt-gdb/lib
valgrind -v --track-fds=yes \
            --trace-children=yes \
            --track-origins=no \
            --undef-value-errors=no \
            --leak-check=full \
            --show-reachable=yes \
            --leak-resolution=high $ADD \
            ../../$BUILD_DIR/QtMjpegVideoSurveillance 2>&1 | tee valgrind.txt
