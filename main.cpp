#include <QtGui/QApplication>

#include "mainwindow.h"
#include "settings.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    a.setOrganizationName("HatredsLogPlace");
    a.setOrganizationDomain("http://hatred.homelinux.net");

    // Load internal settings, distributed with application
    Settings::instance()->load(":/settings/settings-internal.ini");
    // Load user's settings
    Settings::instance()->load();

    MainWindow w;
    w.show();

    int result = a.exec();

    // Optional? Store settings
    Settings::instance()->save();

    return result;
}
